package com.bigcontrols.tokenseller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TokenSellerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TokenSellerApplication.class, args);
	}
}
