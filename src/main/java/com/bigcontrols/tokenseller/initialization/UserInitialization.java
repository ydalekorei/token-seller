package com.bigcontrols.tokenseller.initialization;

import com.bigcontrols.tokenseller.model.Role;
import com.bigcontrols.tokenseller.model.User;
import com.bigcontrols.tokenseller.persistence.RoleRepository;
import com.bigcontrols.tokenseller.persistence.UserRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserInitialization implements InitializingBean {

    private static final String USERNAME = "bigcontrolsadmin";

    private static final String PASSWORD = "$2a$10$JgZofAVIALH9pYprMLl5xOywRAGBKngK28yWFqPJTEpGWzKEGV82u";

    private static final String ADMIN_ROLE = "ROLE_ADMIN";

    private static final String USER_ROLE = "ROLE_USER";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void afterPropertiesSet() throws Exception {
        Role adminRole = getOrCreate(ADMIN_ROLE);
        Role userRole = getOrCreate(USER_ROLE);

        User user = new User();
        user.setUsername(USERNAME);
        user.setPassword(PASSWORD);
        user.getRoles().add(adminRole);
        user.getRoles().add(userRole);

        createIfNotExists(user);
    }

    private Role getOrCreate(String name) {
        Optional<Role> role = roleRepository.findByName(name);
        return role.orElseGet(() -> roleRepository.saveAndFlush(new Role(name)));
    }

    private void createIfNotExists(User userToCreate) {
        userRepository.findByUsername(userToCreate.getUsername())
                .orElseGet(() -> userRepository.saveAndFlush(userToCreate));
    }
}
