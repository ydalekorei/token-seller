package com.bigcontrols.tokenseller.integration.bitcoin;

import java.util.Set;

public class AddressDto {
    public String address;
    public Long total_received;
    public Long total_sent;
    public Long balance;
    public Long unconfirmed_balance;
    public Long final_balance;
    public Long n_tx;
    public Long unconfirmed_n_tx;
    public Long final_n_tx;
    public Set<TransactionDto> txs;
}
