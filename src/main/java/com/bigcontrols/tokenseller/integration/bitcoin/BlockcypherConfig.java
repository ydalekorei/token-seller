package com.bigcontrols.tokenseller.integration.bitcoin;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BlockcypherConfig {
    @Value("${blockcypher.url}")
    public String url;
    @Value("${blockcypher.address}")
    public String address;
}
