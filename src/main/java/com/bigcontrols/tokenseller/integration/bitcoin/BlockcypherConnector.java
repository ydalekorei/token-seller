package com.bigcontrols.tokenseller.integration.bitcoin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class BlockcypherConnector {

    private String url;

    private RestTemplate restTemplate;

    @Autowired
    public BlockcypherConnector(BlockcypherConfig blockcypherConfig, RestTemplateBuilder restTemplateBuilder) {
        this.url = blockcypherConfig.url;
        this.restTemplate = restTemplateBuilder.build();
    }

    public AddressDto getAddressWithTransactions() {
        return restTemplate.getForObject(url, AddressDto.class);
    }
}
