package com.bigcontrols.tokenseller.integration.bitcoin;

import java.util.Set;

public class TransactionDto {
    public String block_hash;
    public Long block_height;
    public String hash;
    public Set<String> addresses;
    public Long total;
    public Long fees;
    public Long size;
    public String preference;
    public String relayed_by;
    //Change to Date
    public String confirmed;
    //Change to Date
    public String received;
    public Long ver;
    public Long lock_time;
    public Boolean double_spend;
    public Long vin_sz;
    public Long vout_sz;
    public Long confirmations;
    public Long confidence;
    public Set<TransactionInputDto> inputs;
    public Set<TransactionOutputDto> outputs;
}
