package com.bigcontrols.tokenseller.integration.bitcoin;

import java.util.Set;

public class TransactionInputDto {
    public String prev_hash;
    public Long output_index;
    public String script;
    public Long output_value;
    public Long sequence;
    public Set<String> addresses;
    public String script_type;
}
