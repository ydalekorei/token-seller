package com.bigcontrols.tokenseller.integration.bitcoin;

import java.util.Set;

public class TransactionOutputDto {
    public Long value;
    public String script;
    public Set<String> addresses;
    public String script_type;
}
