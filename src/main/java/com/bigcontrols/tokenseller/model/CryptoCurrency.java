package com.bigcontrols.tokenseller.model;

public enum CryptoCurrency {

    BITCOIN("BTC");

    private String code;

    CryptoCurrency(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
