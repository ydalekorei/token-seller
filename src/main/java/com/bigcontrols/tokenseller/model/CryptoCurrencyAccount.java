package com.bigcontrols.tokenseller.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class CryptoCurrencyAccount {

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "currency", nullable = false)
    @Enumerated(EnumType.STRING)
    private CryptoCurrency currency;

    public CryptoCurrencyAccount() {
    }

    public CryptoCurrencyAccount(CryptoCurrency currency, String address) {
        this.address = address;
        this.currency = currency;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public CryptoCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(CryptoCurrency currency) {
        this.currency = currency;
    }
}
