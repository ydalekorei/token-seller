package com.bigcontrols.tokenseller.model;

import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "investors")
@EnableJpaAuditing
public class Investor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String ethereumAddress;

    @ElementCollection
    @CollectionTable(name = "crypto_currency_account", joinColumns = @JoinColumn(name = "investor_id"))
    private Set<CryptoCurrencyAccount> cryptoCurrencyAccounts = new HashSet<>();

    public Investor() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEthereumAddress() {
        return ethereumAddress;
    }

    public void setEthereumAddress(String ethereumAddress) {
        this.ethereumAddress = ethereumAddress;
    }

    public Set<CryptoCurrencyAccount> getCryptoCurrencyAccounts() {
        return cryptoCurrencyAccounts;
    }

    public void setCryptoCurrencyAccounts(Set<CryptoCurrencyAccount> cryptoCurrencyAccounts) {
        this.cryptoCurrencyAccounts = cryptoCurrencyAccounts;
    }
}
