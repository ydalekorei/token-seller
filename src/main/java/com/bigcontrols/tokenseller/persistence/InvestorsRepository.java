package com.bigcontrols.tokenseller.persistence;

import com.bigcontrols.tokenseller.model.Investor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvestorsRepository extends JpaRepository<Investor, Long> {
}
