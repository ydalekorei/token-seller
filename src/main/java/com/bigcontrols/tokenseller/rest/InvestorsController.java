package com.bigcontrols.tokenseller.rest;

import com.bigcontrols.tokenseller.model.CryptoCurrencyAccount;
import com.bigcontrols.tokenseller.model.Investor;
import com.bigcontrols.tokenseller.model.CryptoCurrency;
import com.bigcontrols.tokenseller.persistence.InvestorsRepository;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/investors")
public class InvestorsController {

    @Autowired
    private InvestorsRepository investorsRepository;

    @PostMapping(value = "/import/csv")
    public ResponseEntity<String> importAccountsFromCsv(HttpServletRequest request) {

        List<Investor> importedInvestors = new ArrayList<>();

        try {
            Reader in  = new InputStreamReader(request.getInputStream());
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : records) {
                Investor investor = new Investor();
                investor.setEthereumAddress(record.get("ETH"));
                Set<CryptoCurrencyAccount> cryptoCurrencyAccounts = new HashSet<>();
                for (CryptoCurrency cryptoCurrency: CryptoCurrency.values()) {
                    if (record.isSet(cryptoCurrency.getCode())) {
                        cryptoCurrencyAccounts.add(
                                new CryptoCurrencyAccount(cryptoCurrency, record.get(cryptoCurrency.getCode())));
                    }
                }

                investor.setCryptoCurrencyAccounts(cryptoCurrencyAccounts);
                importedInvestors.add(investor);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        investorsRepository.saveAll(importedInvestors);

        return ResponseEntity.ok("Uploaded");

    }
}
