package com.bigcontrols.tokenseller.rest;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestDemoController {
    @RequestMapping(value = "/hello")
    public ResponseEntity<String> helloUser() {

        return ResponseEntity.ok("Hello from secured endpoint");
    }
}
