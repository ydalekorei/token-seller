package com.bigcontrols.tokenseller.security;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HttpStatusReturningAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private final HttpStatus httpStatusToReturn;

    public HttpStatusReturningAuthenticationFailureHandler(HttpStatus httpStatusToReturn) {
        this.httpStatusToReturn = httpStatusToReturn;
    }

    public HttpStatusReturningAuthenticationFailureHandler() {
        this.httpStatusToReturn = HttpStatus.UNAUTHORIZED;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        response.setStatus(this.httpStatusToReturn.value());
        response.getWriter().flush();
    }
}
