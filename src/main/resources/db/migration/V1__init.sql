CREATE SEQUENCE hibernate_sequence START 1 INCREMENT 1;

CREATE TABLE users
(
  id bigint NOT NULL,
  password character varying(255),
  username character varying(255),
  CONSTRAINT users_pkey PRIMARY KEY (id)
);

CREATE TABLE roles
(
  id bigint NOT NULL,
  name character varying(255),
  CONSTRAINT roles_pkey PRIMARY KEY (id)
);

CREATE TABLE users_roles
(
  user_id bigint NOT NULL,
  role_id bigint NOT NULL,
  CONSTRAINT user_role_pkey PRIMARY KEY (user_id, role_id),
  CONSTRAINT fkj345gk1bovqvfame88rcx7yyx FOREIGN KEY (user_id)
      REFERENCES users (id),
  CONSTRAINT fkt7e7djp752sqn6w22i6ocqy6q FOREIGN KEY (role_id)
      REFERENCES roles (id)
);
