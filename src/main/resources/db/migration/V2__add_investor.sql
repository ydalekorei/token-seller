CREATE TABLE investors
(
  id bigint NOT NULL,
  ethereum_address varchar(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE crypto_currency_account
(
  investor_id bigint NOT NULL,
  address varchar(255) NOT NULL,
  currency varchar(255) NOT NULL,
  PRIMARY KEY (investor_id, address, currency),
  CONSTRAINT FKljxbsekkxyh1wdic51m2jevh FOREIGN KEY (investor_id) REFERENCES investors
);
