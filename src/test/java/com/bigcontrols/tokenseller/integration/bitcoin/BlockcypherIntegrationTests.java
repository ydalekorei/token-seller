package com.bigcontrols.tokenseller.integration.bitcoin;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BlockcypherIntegrationTests {

	@Autowired
	public BlockcypherConnector blockcypherConnector;

	@Test
	public void contextLoads() {
		AddressDto addressDto = blockcypherConnector.getAddressWithTransactions();

		assertThat(addressDto.address).isNotNull();
	}

}
