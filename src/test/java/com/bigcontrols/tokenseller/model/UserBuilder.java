package com.bigcontrols.tokenseller.model;

import com.google.common.collect.Sets;

import java.util.HashSet;
import java.util.Set;

import static com.bigcontrols.tokenseller.model.RoleBuilder.aRole;

public final class UserBuilder {
    private Long id;
    private String username = "testuser";
    private String password = "testpassword";
    private Set<Role> roles = Sets.newHashSet(aRole().build());

    private UserBuilder() {
    }

    public static UserBuilder anUser() {
        return new UserBuilder();
    }

    public UserBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public UserBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public UserBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder withRoles(Set<Role> roles) {
        this.roles = roles;
        return this;
    }

    public User build() {
        User user = new User();
        user.setId(id);
        user.setUsername(username);
        user.setPassword(password);
        user.setRoles(roles);
        return user;
    }
}
