package com.bigcontrols.tokenseller.persistence;

import com.bigcontrols.tokenseller.model.Role;
import com.bigcontrols.tokenseller.model.User;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static com.bigcontrols.tokenseller.model.RoleBuilder.aRole;
import static com.bigcontrols.tokenseller.model.UserBuilder.anUser;
import static com.google.common.collect.Sets.newHashSet;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void findByUsername() {
        Role role = aRole().build();
        User user = anUser().withRoles(newHashSet(role)).build();
        entityManager.persistAndFlush(role);

        User expectedUser = entityManager.persistFlushFind(user);

        User actualUser = userRepository.findByUsername(expectedUser.getUsername()).get();

        assertThat(actualUser.getUsername()).isEqualTo(expectedUser.getUsername());
    }
}