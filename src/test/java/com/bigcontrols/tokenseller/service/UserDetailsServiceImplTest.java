package com.bigcontrols.tokenseller.service;

import com.bigcontrols.tokenseller.model.Role;
import com.bigcontrols.tokenseller.model.User;
import com.bigcontrols.tokenseller.persistence.UserRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static com.bigcontrols.tokenseller.model.RoleBuilder.aRole;
import static com.bigcontrols.tokenseller.model.UserBuilder.anUser;
import static com.google.common.collect.Sets.newHashSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

public class UserDetailsServiceImplTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private UserRepository userRepository;

    private UserDetailsService userDetailsService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        userDetailsService = new UserDetailsServiceImpl(userRepository);
    }

    @Test
    public void loadUserByUsernameShouldThrowExceptionIfUserNotFound() throws Exception {
        given(userRepository.findByUsername(anyString())).willReturn(Optional.empty());
        thrown.expect(UsernameNotFoundException.class);
        thrown.expectMessage("missinguser");

        userDetailsService.loadUserByUsername("missinguser");
    }

    @Test
    public void loadUserByUsernameShouldReturnUserIfExists() throws Exception {
        Role expectedRole = aRole().withName("ROLE_TEST").build();
        User expectedUser = anUser().withRoles(newHashSet(expectedRole)).build();
        given(userRepository.findByUsername(expectedUser.getUsername())).willReturn(Optional.of(expectedUser));

        UserDetails actualUser = userDetailsService.loadUserByUsername(expectedUser.getUsername());

        assertThat(actualUser.getUsername()).isEqualTo(expectedUser.getUsername());
        assertThat(actualUser.getAuthorities().iterator().next().getAuthority()).isEqualTo(expectedRole.getName());
    }
}